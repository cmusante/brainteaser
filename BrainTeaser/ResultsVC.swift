//
//  ResultsVC.swift
//  BrainTeaser
//
//  Created by Christopher Musante on 6/1/16.
//  Copyright © 2016 Hypertapp. All rights reserved.
//

import UIKit

class ResultsVC: UIViewController {
    
    
    
    @IBOutlet weak var numCorrect: UILabel!
    @IBOutlet weak var numIncorrect: UILabel!
    @IBOutlet weak var totalCards: UILabel!
    
    @IBAction func playAgian (sender: UIButton) {
        
        
        
    }
    
    
    var correctReceiver = 0
    var incorrectReceiver = 0
    var cardCounterReceiver = 1
    
    
    override func viewDidLoad() {
        numCorrect.text = String(correctReceiver)
        numIncorrect.text = String(incorrectReceiver)
        totalCards.text = String(cardCounterReceiver)
        
    }
    
}