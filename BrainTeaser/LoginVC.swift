//
//  ViewController.swift
//  BrainTeaser
//
//  Created by Christopher Musante on 5/30/16.
//  Copyright © 2016 Hypertapp. All rights reserved.
//

import UIKit
import pop

class LoginVC: UIViewController {
    
    
    @IBOutlet weak var emailConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginConstraint: NSLayoutConstraint!
    
    var animEngine: AnimationEngine!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.animEngine = AnimationEngine(constraints: [emailConstraint, passwordConstraint, loginConstraint])
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(animated: Bool) {
       
        self.animEngine.animateOnScreen(1)
    }

}

