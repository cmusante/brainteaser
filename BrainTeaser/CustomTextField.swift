//
//  CustomTextField.swift
//  BrainTeaser
//
//  Created by Christopher Musante on 5/31/16.
//  Copyright © 2016 Hypertapp. All rights reserved.
//

import UIKit


@IBDesignable
class CustomTextField : UITextField {
    
    
    @IBInspectable var inset: CGFloat = 0
    
    // Rounding corners of text fields
    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        
        didSet {
            
            
            setupView()
        }
    }
    
    //Sets style for text field while not editing
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        
         return CGRectInset(bounds, inset, inset)
        
        
    }
    
    //Sets style for text field while editing
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        
        return textRectForBounds(bounds)
        
        
    }
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 0.5
        
    }
    
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
         
        setupView()
    }
    
    func setupView() {
        
        self.layer.cornerRadius = cornerRadius
        
    }
}
