//
//  GameVC.swift
//  BrainTeaser
//
//  Created by Christopher Musante on 5/31/16.
//  Copyright © 2016 Hypertapp. All rights reserved.
//

import UIKit
import pop

class GameVC: UIViewController {
    
    @IBOutlet weak var yesBtn: CustomButton!
    @IBOutlet weak var noBtn: CustomButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    var timer = NSTimer()
    
    var seconds = 60
    
    var correct = 0
    var incorrect = 0
    var cardCounter = -1
    
    var currentCard: Card!
    var previousCard: Card!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentCard = createCardFromNib()
        
        previousCard = currentCard
        
        currentCard.center = AnimationEngine.screenCenterPosition
        self.view.addSubview(currentCard)
        
    
    }
    
    func countDown() {
        
        seconds -= 1
        timerLabel.text = "00:\(seconds)"
        
        if seconds == 0 {
            
            timer.invalidate()
            seconds = 0
            showResults()
            
            
        }
        
    }
    
    func startTimer() {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(GameVC.countDown), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        
        timer.invalidate()
    }
    
    
    
    @IBAction func yesPressed(sender: UIButton) {
        if sender.titleLabel?.text == "START" {
           startTimer()

        } else {
            
         titleLbl.text = "Does this card match the previous?"
            checkAnswer()
        
           
        }
        showNextCard()
        cardCounter += 1
    }
    
    
    @IBAction func noPressed(sender: AnyObject) {
        showNextCard()
        checkAnswer()
       
        cardCounter += 1
    }
    
    func showNextCard() {
        
        if let current = currentCard {
            let cardToRemove = current
            previousCard = cardToRemove
            currentCard = nil
            
            AnimationEngine.animateToPosition(cardToRemove, position: AnimationEngine.offScreenLeftPosition, completion: { (anim:POPAnimation!, finished: Bool) in
                
                cardToRemove.removeFromSuperview()
            })
            
        }
        
        if let next = createCardFromNib() {
            next.center = AnimationEngine.offScreenRightPosition
            self.view.addSubview(next)
            currentCard = next
            
            if noBtn.hidden {
                noBtn.hidden = false
                yesBtn.setTitle("YES", forState: .Normal)
            }
            
            AnimationEngine.animateToPosition(next, position: AnimationEngine.screenCenterPosition, completion:  { (anim: POPAnimation!, finished: Bool) -> Void in
                
            })
        }
    }
    
    func showResults() {
        performSegueWithIdentifier("showResults", sender: self)
    }
    
    func createCardFromNib() -> Card! {
        return NSBundle.mainBundle().loadNibNamed("Card", owner: self, options: nil) [0] as? Card
        
    }
    func checkAnswer() {
        
        if currentCard.currentShape == previousCard.currentShape {
            correct += 1
            
        } else {
            
            incorrect += 1
        }
        
        
   }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? ResultsVC {
            destination.correctReceiver = correct
            destination.incorrectReceiver = incorrect
            destination.cardCounterReceiver = cardCounter
        }
    }

}
